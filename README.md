## Introducción

Este es un ejercicio colaborativo, en el que utilizarás por primera vez una
plataforma de control de versiones con [Git](https://git-scm.com/book/en/v2).

El objetivo es crear un libro en [LaTeX](https://www.latex-project.org/) que
contenga un capítulo escrito por cada uno de los estudiantes de Ciencias de
Computación de la generación 2023. En este capítulo puedes escribir una breve
presentación de ti mismo, incluyendo alguna ilustración que puede ser una foto
de ti o algo que consideres que te representa. Esperamos que este ejercicio
ayude a que la generación 2023 se conozca e identifique más fácilmente. Si no
sabes qué escribir en tu capítulo, aquí hay algunas ideas (sólo escribe la
información con la que te sientas cómodo/a de compartir):

- Nombre y edad.
- De dónde eres.
- Por qué elegiste estudiar Ciencias de la Computación.
- Qué te gusta hacer en tu tiempo libre.
- Alguna afición.
- Acerca de tus mascotas.

## Instrucciones

1. Ve [al repositorio del proyecto
   coolaborativo](https://gitlab.com/CComputacion/generacion2023) y haz clic en
   **Request Access**.

   ![gitlab1.png](https://gitlab.com/CComputacion/generacion2023/-/wikis/uploads/e39876437242cf20fddb00ad8c5e4d8f/gitlab1.png)

2. Ahora haz clic en el botón **Clone** y copia la URL **HTTPS** para clonar el
   repositorio desde la terminal.

   ![gitlab2.png](https://gitlab.com/CComputacion/generacion2023/-/wikis/uploads/857b9e103af147f46572a26c5077ecda/gitlab2.png)

3. Ve a la terminal y clona (`clone`) la URL que acabas de copiar.

    ```
    git clone https://gitlab.com/CComputacion/generacion2023.git
    ```

4. Crea una rama (`branch`) que tenga como nombre tu número de cuenta y pasa a
   trabajar en ella (`checkout`).

    ```
    git branch [no. cuenta]
    git checkout [no. cuenta]
    ```

5. Abre [tu editor](https://www.gnu.org/software/emacs/) y edita el archivo con
   extensión `tex` que se encuentra dentro del directorio que se llama como tu
   número de cuenta. También puedes agregar las ilustraciones que necesites para
   tu texto; sólo recuerda mantenerlas dentro de tu directorio, que corresponde
   a tu capítulo.

6. Cuando hayas terminado tus cambios, salva tus cambios en tu editor, agrega
   (`add`) los cambios a Git y guárdalos (`commit`).

    ```
    git add [no. cuenta]
    git commit -m "Mi capítulo está listo."
    ```

7. Ahora empuja (`push`) tus cambios al repositorio en línea.

    ```
    git push origin [no. cuenta]
    ```

8. Ve a
   [`https://gitlab.com/CComputacion/generacion2023`](https://gitlab.com/CComputacion/generacion2023)
   y selecciona la rama que corresponde a tu capítulo.

   ![gitlab3.png](https://gitlab.com/CComputacion/generacion2023/-/wikis/uploads/9985c47e6524aa622346e3a709104043/gitlab3.png)

9. Ahora haz clic en el botón **Create merge request**.

   ![gitlab4.png](https://gitlab.com/CComputacion/generacion2023/-/wikis/uploads/7131924bb7285c50839993af9a02b6a2/gitlab4.png)

10. El título de tu solicitud de mezcla (`merge request`) es, por omisión, el
    comentario del último cambio guardado en tu rama en Git; agregar una
    descripción o comentarios es opcional. No es necesario que edites nada.

11. Ve a la parte inferior de la página de creación de nueva solicitud de mezcla
    y haz clic en **Create merge request**.

    ![gitlab5.png](https://gitlab.com/CComputacion/generacion2023/-/wikis/uploads/d2d44ca8420d98cd1e091b3d8e0eaf49/gitlab5.png)

12. Ahora espera a que los tutores del propedéutico acepten las solicitudes de
    mezcla; te avisaremos por correo cuando el libro esté listo.

Recuerda no modificar nada fuera del directorio con tu número de cuenta. 🙂
